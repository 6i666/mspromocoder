<?php
// For debug
ini_set('display_errors', 1);
ini_set('error_reporting', -1);

if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php')) {
    /** @noinspection PhpIncludeInspection */
    require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
}
else {
    require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.core.php';
}
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var msPromocoder $msPromocoder */
$msPromocoder = $modx->getService('mspromocoder', 'msPromocoder', $modx->getOption('mspromocoder_core_path', null, $modx->getOption('core_path') . 'components/mspromocoder/') . 'model/mspromocoder/');
$modx->lexicon->load('mspromocoder:default');

// handle request
$corePath = $modx->getOption('mspromocoder_core_path', null, $modx->getOption('core_path') . 'components/mspromocoder/');
$path = $modx->getOption('processorsPath', $msPromocoder->config, $corePath . 'processors/');
$modx->request->handleRequest(array(
	'processors_path' => $path,
	'location' => '',
));
