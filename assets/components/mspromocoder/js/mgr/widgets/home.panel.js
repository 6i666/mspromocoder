msPromocoder.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateId: 'mspromocoder-panel-home',
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('mspromocoder_codes') + '</h2>',
			cls: 'modx-page-header container',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
		  stateEvents: ['tabchange'],
		 	stateful: true,
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
		 	getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
			items: [{
				title: _('mspromocoder_codes_common'),
				layout: 'anchor',
				deferredRender: true,
				items: [{
					html: _('mspromocoder_intro_msg'),
					bodyCssClass: 'panel-desc',
				}, {
					xtype: 'mspromocoder-grid-codes',
					id: 'wwwwwwwwwwwwww',
					cls: 'main-wrapper',
					type: '0'
				}]
			}, {
				title: _('mspromocoder_codes_p'),
				layout: 'anchor',
				deferredRender: true,
				items: [{
					html: _('mspromocoder_intro_msg'),
					bodyCssClass: 'panel-desc',
				}, {
					xtype: 'mspromocoder-grid-codes',
					id: 'qqqqqqqqqqqqqqqq',
					cls: 'main-wrapper',
					type: 1
				}]
			}, {
				title: _('mspromocoder_codes_cv'),
				layout: 'anchor',
				deferredRender: true,
				items: [{
					html: _('mspromocoder_intro_msg'),
					bodyCssClass: 'panel-desc',
				}, {
					xtype: 'mspromocoder-grid-codes',
					id: 'rrrrrrrrrrrrrr',
					cls: 'main-wrapper',
					type: 2
				}]
			}]
		}]
	});
	msPromocoder.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(msPromocoder.panel.Home, MODx.Panel);
Ext.reg('mspromocoder-panel-home', msPromocoder.panel.Home);
