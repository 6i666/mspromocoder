msPromocoder.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'mspromocoder-panel-home', renderTo: 'mspromocoder-panel-home-div'
		}]
	});
	msPromocoder.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(msPromocoder.page.Home, MODx.Component);
Ext.reg('mspromocoder-page-home', msPromocoder.page.Home);