<?php

/**
 * The home manager controller for msPromocoder.
 *
 */
class msPromocoderHomeManagerController extends msPromocoderMainController {
	/* @var msPromocoder $msPromocoder */
	public $msPromocoder;


	/**
	 * @param array $scriptProperties
	 */
	public function process(array $scriptProperties = array()) {
	}


	/**
	 * @return null|string
	 */
	public function getPageTitle() {
		return $this->modx->lexicon('mspromocoder');
	}


	/**
	 * @return void
	 */
	public function loadCustomCssJs() {
		$this->addCss($this->msPromocoder->config['cssUrl'] . 'mgr/main.css');
		$this->addCss($this->msPromocoder->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
		$this->addJavascript($this->msPromocoder->config['jsUrl'] . 'mgr/misc/utils.js');
		$this->addJavascript($this->msPromocoder->config['jsUrl'] . 'mgr/widgets/home.panel.js');
		$this->addJavascript($this->msPromocoder->config['jsUrl'] . 'mgr/sections/home.js');
		// Promocodes part.
		$this->addJavascript($this->msPromocoder->config['jsUrl'] . 'mgr/widgets/codes/codes.grid.js');
		$this->addJavascript($this->msPromocoder->config['jsUrl'] . 'mgr/widgets/codes/codes.windows.js');

		$this->addHtml('<script type="text/javascript">
		Ext.onReady(function() {
			MODx.load({ xtype: "mspromocoder-page-home"});
		});
		</script>');
	}


	/**
	 * @return string
	 */
	public function getTemplateFile() {
		return $this->msPromocoder->config['templatesPath'] . 'home.tpl';
	}
}