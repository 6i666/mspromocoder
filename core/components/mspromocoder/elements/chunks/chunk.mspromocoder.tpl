<div class="form-group input-parent">
    <label class="col-sm-4 control-label" for="coupon_code">Проверка купона/промокода</label>
    <div class="col-sm-6">
        <input type="text" id="msPromocoder-check" placeholder="Введите промокод" name="mspromocoder_check" value="[[+mspromocoder_check]]" class="form-control">
    </div>
</div>
<div class="form-group input-parent">
    <label class="col-sm-4 control-label" for="coupon_code">Промокод</label>
    <div class="col-sm-6">
        <input type="text" id="msPromocoder-code" placeholder="Введите промокод" name="mspromocoder_code" value="[[+mspromocoder_code]]" class="form-control">
    </div>
</div>

<script>
(function($) {
    $(document).ready(function() {
        // Update cart total price with discount.
        var cart_total_price = $('.ms2_total_cost');
        if (cart_total_price.length > 0) {
            $.post('/assets/components/mspromocoder/action.php', {
                action: 'cart/with_discont',
                only_cart: true,
                promocode: '',
            },
            function(data) {
                refresh_cart(data);
            });
        }

        // Function for replace products price with new.
        function refresh_cart(data) {
            if (data == false) {
                return;
            }
            data = JSON.parse(data);

            $.each(data.products, function(key, item) {
                price = '<span>' + miniShop2.Utils.formatPrice(item['price']) + '</span> руб.';
                if (item['old_price'] != '') {
                    price += '<br><span class="old_price">' + miniShop2.Utils.formatPrice(item['old_price']) + ' руб.</span>';
                }
                $('#' + key + ' .price').html(price);
            });

            miniShop2.Cart.status(data['status']);

            return;
        }

        // Minishop callback on promocode add.
        miniShop2.Callbacks.Order.add.ajax.done = function(res) {
            // var res = res.responseJSON; // Fore modx > 2.2
            var res = $.parseJSON(res.responseText); // For modx 2.2
            if (typeof(res.data['mspromocoder_check']) != 'undefined') {
                var code = res.data['mspromocoder_check'],
                    promocode_field = $('#msPromocoder-code'),
                    coupon_field = $('#coupon_code');

                coupon_field.val('').trigger('change');
                promocode_field.val('').trigger('change');

                $.post('/assets/components/mspromocoder/action.php', {
                    action: 'check/code',
                    promocode: code
                },
                function(check) {
                    if (check == 'coupon') {
                        coupon_field.val(code).trigger('change');
                    }
                    else {
                        promocode_field.val(code).trigger('change');
                    }
                });
            }
            if (typeof(res.data['mspromocoder_code']) != 'undefined') {
                miniShop2.Order.getcost();
                action = 'cart/with_discont';
                if (res.data['mspromocoder_code'] == '') {
                    action = 'cart/without_discont';
                }

                $.post('/assets/components/mspromocoder/action.php', {
                    action: action,
                    only_cart: true,
                    promocode: res.data['mspromocoder_code']
                },
                function(data) {
                    refresh_cart(data);
                });
            }
        }
    });
})(jQuery);
</script>