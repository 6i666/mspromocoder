<?php
/**
 * The base class for msPromocoder.
 */

class msPromocoder {
	public $modx;
	public $miniShop2;
	public $mSearch2;
	public $error_message;

	/**
	 * @param modX $modx
	 * @param array $config
	 */
	function __construct(modX &$modx, array $config = array()) {
		$this->modx =& $modx;
	  $this->miniShop2 = $this->modx->getService('miniShop2');
	  $this->miniShop2->initialize($this->modx->context->key);
	  $this->mSearch2 = false;
	  if ($modx->loadClass('msearch2', MODX_CORE_PATH . 'components/msearch2/model/msearch2/', false, true)) {
	  	$this->mSearch2 = $this->modx->getService('mSearch2');
	  }

		$corePath = $this->modx->getOption('mspromocoder_core_path', $config, $this->modx->getOption('core_path') . 'components/mspromocoder/');
		$assetsUrl = $this->modx->getOption('mspromocoder_assets_url', $config, $this->modx->getOption('assets_url') . 'components/mspromocoder/');
		$connectorUrl = $assetsUrl . 'connector.php';

		$this->config = array_merge(array(
			'assetsUrl' => $assetsUrl,
			'cssUrl' => $assetsUrl . 'css/',
			'jsUrl' => $assetsUrl . 'js/',
			'imagesUrl' => $assetsUrl . 'images/',
			'connectorUrl' => $connectorUrl,
			'corePath' => $corePath,
			'modelPath' => $corePath . 'model/',
			'chunksPath' => $corePath . 'elements/chunks/',
			'templatesPath' => $corePath . 'elements/templates/',
			'chunkSuffix' => '.chunk.tpl',
			'snippetsPath' => $corePath . 'elements/snippets/',
			'processorsPath' => $corePath . 'processors/'
		), $config);

		$this->modx->addPackage('mspromocoder', $this->config['modelPath']);
		$this->modx->lexicon->load('mspromocoder:default');
	}

	/**
	 * Sanitizes values for processors
	 *
	 * @param $key
	 * @param $value
	 *
	 * @return mixed|string
	 */
	public function sanitize($key, $value) {
		if (!is_array($value)) {
			$value = trim($value);
		}

		switch ($key) {
			case 'discount':
				if (strpos($value, '%') !== false) {
					$value = (int)str_replace(array('%', ' '), '', $value) . '%';
				}
				else {
					$value = (int)$value;
				}
				if (empty($value)) {
					$value = '0%';
				}
				break;
			case 'min_order_price':
				$value = (int)$value;
				if (empty($value)) {
					$value = 0;
				}
				break;
			case 'use_count':
			case 'begins':
			case 'ends':
				if (empty($value)) {
					$value = NULL;
				}
				break;
			case 'products':
			case 'categories':
			case 'vendors':
				if (is_array($value)) {
					foreach ($value as $k => $v) {
						if (empty($v)) {
							unset($value[$k]);
						}
					}
					$value = !empty($value) ? $this->modx->toJSON($value) : NULL;
				}
				break;
		}

		return $value;
	}

	/**
	 * Format date for output in datefield.
	 *
	 * @var mixed $date Date in format 0000-00-00 00:00:00 or NULL.
	 * @return string $output Date in format 'd-m-Y'.
	 */
	function date_format($date) {
		$output = '';
		$date = !is_null($date) ? strtotime($date) : false;

		if ($date) {
			$output = date('d-m-Y', $date);
		}

		return $output;
	}

	/**
	 * Return promocode tepes.
	 *
	 * @return array
	 */
	function promocode_types() {

		// return $output;
	}

	/**
	 * Check and validate promocode.
   *
	 * @var string $code Promocode.
	 *
	 * @return bool|array
	 */
	function check_code($code = '') {
	  // Get promocode fields.
		$promocode = $this->get_code($code);

		if ($promocode === false) {
			return $this->error('Неправильный промокод!');
		}
		if ($promocode['active'] == 0) {
			return $this->error('Промокод не активен!');
		}
		if (!empty($promocode['begins']) && strtotime($promocode['begins']) > time()) {
			return $this->error('Вы сможете воспользоваться промокодом с ' . date('d-m-Y', strtotime($promocode['begins'])));
		}
		if (!empty($promocode['ends']) && strtotime($promocode['ends']) < time()) {
			return $this->error('Промокод больше не действителен!');
		}
		if (!empty($promocode['use_count']) && $promocode['use_count'] <= $promocode['used']) {
			return $this->error('Превышен лимит на использование промокода!');
		}

		$status = $this->miniShop2->cart->status();
		if ($status['total_cost'] < $promocode['min_order_price']) {
			return $this->error('Промокод можно применить при сумме заказа от ' . $promocode['min_order_price'] . 'грн');
		}

		return true;
	}

	/**
	 *	Get promocode fields.
	 *
	 * @var string $code Promocode.
	 *
	 * @return array|bool
	 */
	function get_code($code) {
		$query = $this->modx->newQuery('msPromocoderCodes');
		$query->where(array(
			'code' => $code
		));
		$promocode = $this->modx->getObject('msPromocoderCodes', $query);

		if ($promocode) {
			return $promocode->toArray();
		}

		return false;
	}

	/**
	 *	Get products attached to promocode.
	 *
	 * @var int $code_id Promocode id.
	 *
	 * @return array|bool
	 */
	function get_code_products($code_id) {
		$query = $this->modx->newQuery('msPromocoderProducts');
		$query->where(array(
			'code_id' => $code_id
		));
		$result = $this->modx->getObject('msPromocoderProducts', $query);

		if ($result) {
			$products = $this->modx->fromJSON($result->get('products'));
			if (!empty($products)) {
				return $products;
			}

			return false;
		}

		return false;
	}

	/**
	 * Get categories and vendors attached to promocode.
	 *
	 * @var int $code_id Promocode id.
	 *
	 * @return array|bool
	 */
	function get_code_categories_vendors($code_id) {
		$query = $this->modx->newQuery('msPromocoderCategoriesVendors');
		$query->where(array(
			'code_id' => $code_id
		));
		$result = $this->modx->getObject('msPromocoderCategoriesVendors', $query);

		if ($result) {
			$return = array(
				'categories' => $this->modx->fromJSON($result->get('categories')),
				'vendors' => $this->modx->fromJSON($result->get('vendors'))
			);

			if (empty($return['categories']) && empty($return['vendors'])) {
				return false;
			}

			return $return;
		}

		return false;
	}

	/**
	 * Error message.
	 *
	 * @var string $message Error message.
	 *
	 * @return array
	 */
	function error($message = '') {
		return array(
			'success' => false,
			'message' => $message
		);
	}

	/**
	 * Get order price with discount.
	 *
	 * @param $code
	 * @param $price
	 *
	 * @return float|int
	 */
	public function get_discounted_cost($only_cart = false) {
		$result = $this->apply_promocode($only_cart);

		if ($result) {
			return $result['status']['total_cost'];
		}

		return false;
	}

	public function apply_promocode($only_cart = false) {
		$order = $this->miniShop2->order->get();
		$promocode = $this->get_code($order['mspromocoder_code']);
		$cart = $this->miniShop2->cart->get();
		$status = $this->miniShop2->cart->status();
		$delivery_cost = 0;
		$payment_cost = 0;

		if (!$promocode) {
			return false;
		}

		if ($only_cart === false) {
			/* @var msDelivery $delivery */
			if (!empty($order['delivery']) && $delivery = $this->modx->getObject('msDelivery', $order['delivery'])) {
				$weight_price = $delivery->get('weight_price');

				$cart_weight = $status['total_weight'];
				$delivery_cost += $weight_price * $cart_weight;

				$add_price = $delivery->get('price');
				if (preg_match('/%$/', $add_price)) {
					$add_price = str_replace('%', '', $add_price);
					$add_price = $delivery_cost / 100 * $add_price;
				}
				$delivery_cost += $add_price;
			}
			/* @var msPayment $payment */
			if (!empty($order['payment']) && $payment = $this->modx->getObject('msPayment', $order['payment'])) {
				$payment_cost = $payment->get('price');
			}
		}

		switch ($promocode['type']) {
			case 0:
				$this->remove_discount();
				$status = $this->miniShop2->cart->status();
				$status['total_cost'] = $this->get_discounted_price($status['total_cost'], $promocode['discount']);
				if ($promocode['free_delivery'] != 1) {
					$status['total_cost'] += $delivery_cost;
				}
				$status['total_cost'] += $payment_cost;

				return array(
					'cart' => array(),
					'status' => $status
				);
				break;

			case 1:
				// Get products attached to promocode.
				$promo_products = $this->get_code_products($promocode['id']);

				if (!$promo_products) {
					return false;
				}

				foreach ($cart as $key => &$product) {
					if (isset($product['mspromocoder']) && $product['mspromocoder'] != $promocode['code']) {
						$product = $this->remove_product_discount($product);
					}
					if (!in_array($product['id'], $promo_products) || isset($product['mspromocoder'])) {
						continue;
					}

					// Apply discount to product.
					$product['mspromocoder'] = $promocode['code'];
					$product['discount'] = $promocode['discount'];
					$product = $this->set_product_discount($product);
				}

				$this->miniShop2->cart->set($cart);
				$status = $this->miniShop2->cart->status();
				if ($promocode['free_delivery'] != 1) {
					$status['total_cost'] += $delivery_cost;
				}
				$status['total_cost'] += $payment_cost;

				return array(
					'cart' => $cart,
					'status' => $status
				);

				break;

			case 2:
				// Get categories and vendors attached to promocode.
				$promo_cv = $this->get_code_categories_vendors($promocode['id']);

				if (!$promo_cv) {
					return false;
				}

				foreach ($cart as $key => &$product) {
					if (isset($product['mspromocoder']) && $product['mspromocoder'] != $promocode['code']) {
						$product = $this->remove_product_discount($product);
					}
					elseif (isset($product['mspromocoder']) && $product['mspromocoder'] == $promocode['code']) {
			    	continue;
					}

					// Get category and vendor from product.
					$product_obj = $this->modx->getObject('msProduct', $product['id']);
					if (!$product_obj) {
						unset($cart[$key]);
				    continue;
					}

			    $category = $product_obj->get('parent');
			    $vendor = $product_obj->get('vendor.id');

			    if (
			    		(
			    			!empty($promo_cv['categories']) && !empty($promo_cv['vendors']) &&
			    			(!in_array($category, $promo_cv['categories']) || !in_array($vendor, $promo_cv['vendors']))
			    		) ||
			    		(
			    			empty($promo_cv['categories']) && !empty($promo_cv['vendors']) &&
			    			!in_array($vendor, $promo_cv['vendors'])
			    		) ||
			    		(
			    			!empty($promo_cv['categories']) && empty($promo_cv['vendors']) &&
			    			!in_array($category, $promo_cv['categories'])
			    		)
			    	) {
						$normal_cost += $product['price'] * $product['count'];
			    	continue;
			    }

					$product['mspromocoder'] = $promocode['code'];
					$product['discount'] = $promocode['discount'];
					$product = $this->set_product_discount($product);
					$discounded_cost += $product['price'] * $product['count'];
				}

				$this->miniShop2->cart->set($cart);
				$status = $this->miniShop2->cart->status();
				if ($promocode['free_delivery'] != 1) {
					$status['total_cost'] += $delivery_cost;
				}
				$status['total_cost'] += $payment_cost;

				return array(
					'cart' => $cart,
					'status' => $status
				);

				break;
		}

		return false;
	}

	/**
	 * Get price with discount.
	 *
	 * @param int $price
	 * @param mixed $discount
	 *
	 * @return float|int
	 */
	public function get_normal_cost() {
		$this->remove_discount();
		$status = $this->miniShop2->cart->status();

		return $status['total_cost'];
	}

	/**
	 * Get price with discount.
	 *
	 * @param int $price
	 * @param mixed $discount
	 *
	 * @return float|int
	 */
	public function get_discounted_price($price, $discount) {
		if (strpos($discount, '%') !== false) {
			$res = $price * ((float)$discount / 100);
		}
		else {
			$res = (float)$discount;
		}

		if ($res > 0) {
			$price -= $res;
		}

		return $price;
	}

	/**
	 * Set discount to product.
	 *
	 * @param array $product
	 *
	 * @return array
	 */
	public function set_product_discount(array $product) {
		$product['old_price'] = $product['price'];
		$product['price'] = $this->get_discounted_price($product['price'], $product['discount']);

		return $product;
	}

	/**
	 * Remove discount to product.
	 *
	 * @param array $product
	 *
	 * @return array
	 */
	public function remove_product_discount(array $product) {
		$product['price'] = $product['old_price'];
		unset($product['old_price']);
		unset($product['mspromocoder']);
		unset($product['discount']);

		return $product;
	}

	/**
	 * Remove discount.
	 */
	public function remove_discount() {
		$cart = $this->miniShop2->cart->get();
		foreach ($cart as $key => &$product) {
			if (isset($product['mspromocoder'])) {
				$product = $this->remove_product_discount($product);
			}
		}

		$this->miniShop2->cart->set($cart);
	}

	/**
	 * Get cart with discount.
	 *
	 * @return array
	 */
	public function get_updated_cart($only_cart = false) {
		$result = $this->apply_promocode($only_cart);
		$products = array();

		if (!$result) {
			return false;
		}

		foreach ($result['cart'] as $key => $product) {
			if (!isset($product['mspromocoder'])) {
				continue;
			}

			$products[$key] = array(
				'price' => $product['price'],
				'old_price' => $product['old_price'],
				'cost' => $product['price'] * $product['count']
			);
		}

		return $this->modx->toJSON(array(
			'products' => $products,
			'status' => $result['status']
		));
	}

	/**
	 * Get cart without discount.
	 *
	 * @return array
	 */
	public function get_deault_cart() {
		$cart = $this->miniShop2->cart->get();
		$products = array();

		foreach ($cart as $key => $product) {
			$product_obj = $this->modx->getObject('msProduct', $product['id']);
			$price = $product_obj->get('price');
	    $old_price = $product_obj->get('old_price');
	    $old_price = $old_price > 0 ? $old_price : '';

			$products[$key] = array(
				'price' => $price,
				'old_price' => $old_price,
				'cost' => $price * $cart['count']
			);
		}

		$this->remove_discount();

		return $this->modx->toJSON(array(
			'products' => $products,
			'status' => $this->miniShop2->cart->status()
		));
	}

}