<?php
$xpdo_meta_map['msPromocoderCodes']= array (
  'package' => 'mspromocoder',
  'version' => '1.1',
  'table' => 'ms_promocoder_codes',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (
    'code' => NULL,
    'type' => 0,
    'discount' => '0',
    'min_order_price' => 0,
    'description' => '',
    'createdon' => 'CURRENT_TIMESTAMP',
    'begins' => NULL,
    'ends' => NULL,
    'active' => 0,
    'free_delivery' => 0,
    'use_count' => NULL,
    'used' => 0,
  ),
  'fieldMeta' => 
  array (
    'code' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
      'index' => 'unique',
    ),
    'type' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '3',
      'phptype' => 'int',
      'null' => false,
      'default' => 0,
    ),
    'discount' => 
    array (
      'dbtype' => 'char',
      'precision' => '7',
      'phptype' => 'string',
      'null' => false,
      'default' => '0',
    ),
    'min_order_price' => 
    array (
      'dbtype' => 'int',
      'precision' => '7',
      'phptype' => 'int',
      'attributes' => 'unsigned',
      'null' => true,
      'default' => 0,
    ),
    'description' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'text',
      'null' => true,
      'default' => '',
    ),
    'createdon' => 
    array (
      'dbtype' => 'timestamp',
      'phptype' => 'timestamp',
      'null' => true,
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'begins' => 
    array (
      'dbtype' => 'timestamp',
      'phptype' => 'timestamp',
      'null' => true,
    ),
    'ends' => 
    array (
      'dbtype' => 'timestamp',
      'phptype' => 'timestamp',
      'null' => true,
    ),
    'active' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'phptype' => 'boolean',
      'attributes' => 'unsigned',
      'null' => true,
      'default' => 0,
    ),
    'free_delivery' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'phptype' => 'boolean',
      'attributes' => 'unsigned',
      'null' => true,
      'default' => 0,
    ),
    'use_count' => 
    array (
      'dbtype' => 'smallint',
      'precision' => '4',
      'phptype' => 'int',
      'attributes' => 'unsigned',
      'null' => true,
    ),
    'used' => 
    array (
      'dbtype' => 'smallint',
      'precision' => '4',
      'phptype' => 'int',
      'attributes' => 'unsigned',
      'null' => true,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'code' => 
    array (
      'alias' => 'code',
      'primary' => false,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'code' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'active' => 
    array (
      'alias' => 'active',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'active' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
);
