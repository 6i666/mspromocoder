<?php
$xpdo_meta_map['msPromocoderProducts']= array (
  'package' => 'mspromocoder',
  'version' => '1.1',
  'table' => 'ms_promocoder_products',
  'extends' => 'xPDOObject',
  'fields' => 
  array (
    'code_id' => NULL,
    'products' => NULL,
  ),
  'fieldMeta' => 
  array (
    'code_id' => 
    array (
      'dbtype' => 'int',
      'phptype' => 'int',
      'null' => false,
      'index' => 'pk',
    ),
    'products' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => true,
    ),
  ),
  'indexes' => 
  array (
    'PRIMARY' => 
    array (
      'alias' => 'PRIMARY',
      'primary' => true,
      'unique' => true,
      'columns' => 
      array (
        'code_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'msPromocoderProducts' => 
    array (
      'class' => 'msPromocoderCodes',
      'local' => 'code_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
