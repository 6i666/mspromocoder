<?php
$xpdo_meta_map['msPromocoderOrders']= array (
  'package' => 'mspromocoder',
  'version' => '1.1',
  'table' => 'ms_promocoder_orders',
  'extends' => 'xPDOObject',
  'fields' => 
  array (
    'code_id' => NULL,
    'order_id' => NULL,
  ),
  'fieldMeta' => 
  array (
    'code_id' => 
    array (
      'dbtype' => 'int',
      'phptype' => 'int',
      'null' => false,
    ),
    'order_id' => 
    array (
      'dbtype' => 'int',
      'phptype' => 'int',
      'null' => false,
    ),
  ),
);
