<?php

/**
 * Seve products attached to promocode.
 */
class msPromocoderProductsUpdateProcessor extends modObjectUpdateProcessor {
  public $objectType = 'msPromocoderProducts';
  public $classKey = 'msPromocoderProducts';
  public $primaryKeyField = 'code_id';

}

return 'msPromocoderProductsUpdateProcessor';