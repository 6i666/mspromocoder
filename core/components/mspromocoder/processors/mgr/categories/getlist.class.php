<?php

/**
 * Get a list of Products.
 */
class msPromocoderProductsGetListProcessor extends modObjectGetListProcessor {

  /**
   * {@inheritDoc}
   * @return mixed
   */
  public function process() {
    return $this->getCategories();
  }

  /**
   * Get the data of the query.
   *
   * @return string (json)
   */
  public function getCategories() {
    $msPromocoder = $this->modx->getService('msPromocoder');
    $query = trim($this->getProperty('query'));

    if ($this->getProperty('valuesqry') == true) {
      $query = explode('|', $query);

      $q = $this->modx->newQuery('modResource');
      $q->where(array(
        'id:IN' => $query,
        'class_key' => 'msCategory'
      ));
      $q->limit(0);
      $q->select('id,pagetitle,pagetitle as value');
      $array = array();
      if ($q->prepare() && $q->stmt->execute()) {
        $array = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
      }

      return $this->outputArray($array, count($array));
    }
    else {
      // Run minishop2 processor to get product categories.
      $response = $this->modx->runProcessor(
        'mgr/category/getlist',
        array(
          'query' => $query
        ),
        array(
          'processors_path' => MODX_CORE_PATH . 'components/minishop2/processors/'
      ));

      $response = $this->modx->fromJSON($response->response);
      foreach ($response->results as $k => $p) {
        $category = new stdClass();
        $category->id = $p->id;
        $category->value = "[{$p->id}] " . $p->pagetitle;
        $category->pagetitle = "[{$p->id}] " . $p->pagetitle;

        $response->results[$k] = $category;
        unset($category);
      }

      return $this->modx->toJSON($response);
    }
  }

}

return 'msPromocoderProductsGetListProcessor';