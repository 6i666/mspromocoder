<?php

/**
 * Update an Code
 */
class msPromocoderCodesUpdateProcessor extends modObjectUpdateProcessor {
	public $objectType = 'msPromocoderCodes';
	public $classKey = 'msPromocoderCodes';


	/**
	 * We doing special check of permission
	 * because of our objects is not an instances of modAccessibleObject
	 *
	 * @return bool|string
	 */
	public function beforeSave() {
		if (!$this->checkPermissions()) {
			return $this->modx->lexicon('access_denied');
		}

		return true;
	}

	/**
	 * @return bool
	 */
	public function beforeSet() {
		$msPromocoder = $this->modx->getService('msPromocoder');
		$properties = $this->getProperties();
		// Sanitize form values before update.
		foreach ($properties as $k => $v) {
			$properties[$k] = $msPromocoder->sanitize($k, $v);
		}

		$id = $properties['id'];
		$code = $properties['code'];

		if (empty($id)) {
			return $this->modx->lexicon('mspromocoder_code_err_ns');
		}

		if (empty($code)) {
			$this->modx->error->addField('code', $this->modx->lexicon('mspromocoder_code_err_code'));
		}
		elseif ($this->modx->getCount($this->classKey, array('code' => $code, 'id:!=' => $id))) {
			$this->modx->error->addField('code', $this->modx->lexicon('mspromocoder_code_err_ae'));
		}

		$this->setProperties($properties);

		switch ($properties['type']) {
			case 1:
				if (empty($properties['products'])) {
					$this->modx->error->addField('products[]', $this->modx->lexicon('mspromocoder_products_err_empty'));
				}
		    // Run processor to update products attached to promocode.
		    $response = $this->modx->runProcessor(
		    	'mgr/products/update',
		    	array(
			      'code_id' => $id,
			      'products' => $properties['products']
			    ),
		    	array(
		      	'processors_path' => $msPromocoder->config['processorsPath']
		    ));
				break;
			case 2:
				if (empty($properties['vendors']) && empty($properties['categories'])) {
					$this->modx->error->addField('vendors[]', $this->modx->lexicon('mspromocoder_vc_err_empty'));
					$this->modx->error->addField('categories[]', $this->modx->lexicon('mspromocoder_vc_err_empty'));
				}

				$cv_table = $this->modx->getTableName('msPromocoderCategoriesVendors');
				$result = $this->modx->query("UPDATE $cv_table SET categories='{$properties['categories']}', vendors='{$properties['vendors']}' WHERE code_id=$id");
				break;
		}

		return parent::beforeSet();
	}
}

return 'msPromocoderCodesUpdateProcessor';
