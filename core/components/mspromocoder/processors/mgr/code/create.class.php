<?php

/**
 * Create an Code
 */
class msPromocoderCodesCreateProcessor extends modObjectCreateProcessor {
	public $objectType = 'msPromocoderCodes';
	public $classKey = 'msPromocoderCodes';
	// public $languageTopics = array('mspromocoder');
	// public $permission = 'create';


	/**
	 * @return bool
	 */
	public function beforeSet() {
		$msPromocoder = $this->modx->getService('msPromocoder');
		$properties = $this->getProperties();

		// Sanitize form values before save.
		foreach ($properties as $k => $v) {
			$properties[$k] = $msPromocoder->sanitize($k, $v);
		}

		$code = $properties['code'];
		if (empty($code)) {
			$this->modx->error->addField('code', $this->modx->lexicon('mspromocoder_code_err_code'));
		}
		elseif ($this->modx->getCount($this->classKey, array('code' => $code))) {
			$this->modx->error->addField('code', $this->modx->lexicon('mspromocoder_code_err_ae'));
		}

		switch ($properties['type']) {
			case 1:
				if (empty($properties['products'])) {
					$this->modx->error->addField('products[]', $this->modx->lexicon('mspromocoder_products_err_empty'));
				}
				break;
			case 2:
				if (empty($properties['vendors']) && empty($properties['categories'])) {
					$this->modx->error->addField('vendors[]', $this->modx->lexicon('mspromocoder_vc_err_empty'));
					$this->modx->error->addField('categories[]', $this->modx->lexicon('mspromocoder_vc_err_empty'));
				}
				break;
		}


		$this->setProperties($properties);

		return parent::beforeSet();
	}

	/**
	 * Override in your derivative class to do functionality after save() is run
	 * @return boolean
	 */
	public function afterSave() {
		$msPromocoder = $this->modx->getService('msPromocoder');
		$code = $this->object->toArray();
		$type = $products = $this->getProperty('type');

		if ($type == 1) {
	    // Run processor to save products attached to promocode.
	    $response = $this->modx->runProcessor(
	    	'mgr/products/create',
	    	array(
		      'code_id' => $code['id'],
		      'products' => $this->getProperty('products')
		    ),
		    array(
	      	'processors_path' => $msPromocoder->config['processorsPath']
	    ));
		}
		elseif ($type == 2) {
	    // Run processor to save products attached to promocode.
	    $response = $this->modx->runProcessor(
	    	'mgr/categories_vendors/create',
	    	array(
		      'code_id' => $code['id'],
		      'vendors' => $this->getProperty('vendors'),
		      'categories' => $this->getProperty('categories')
		    ),
		    array(
	      	'processors_path' => $msPromocoder->config['processorsPath']
	    ));
		}

		return true;
	}

}

return 'msPromocoderCodesCreateProcessor';