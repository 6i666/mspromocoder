<?php

/**
 * Get an Code
 */
class msPromocoderCodesGetProcessor extends modObjectGetProcessor {
	public $objectType = 'msPromocoderCodes';
	public $classKey = 'msPromocoderCodes';


	/**
	 * We doing special check of permission
	 * because of our objects is not an instances of modAccessibleObject
	 *
	 * @return mixed
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}
		$msPromocoder = $this->modx->getService('msPromocoder');
		$data = $this->object->toArray();


		$data['begins'] = $msPromocoder->date_format($data['begins']);
		$data['ends'] = $msPromocoder->date_format($data['ends']);
		$data['createdon'] = $msPromocoder->date_format($data['createdon']);


		switch ($data['type']) {
			case 1:
				// Run msearch2 processor to get products.
				$data['products'] = array();
		    $response = $this->modx->runProcessor(
		    	'mgr/products/get',
		    	array(
			      'code_id' => $data['id']
			    ),
			    array(
		      	'processors_path' => $msPromocoder->config['processorsPath']
		    ));
		    if ($response->response['success'] === true) {
					$data['products[]'] = $this->modx->fromJSON($response->response['object']['products']);
		    }
				break;

			case 2:
				$data['vendors[]'] = array();
				$data['categories[]'] = array();
				$response = $this->modx->runProcessor(
		    	'mgr/categories_vendors/get',
		    	array(
			      'code_id' => $data['id']
			    ),
			    array(
		      	'processors_path' => $msPromocoder->config['processorsPath']
		    ));
		    if ($response->response['success'] === true) {
					$data['vendors[]'] = $this->modx->fromJSON($response->response['object']['vendors']);
					$data['categories[]'] = $this->modx->fromJSON($response->response['object']['categories']);
		    }
				break;
		}

		return $this->success('', $data);
	}

}

return 'msPromocoderCodesGetProcessor';