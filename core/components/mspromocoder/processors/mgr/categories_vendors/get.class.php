<?php

/**
 * Get an Code
 */
class msPromocoderCategoriesVendorsGetProcessor extends modObjectGetProcessor {
  public $objectType = 'msPromocoderCategoriesVendors';
  public $classKey = 'msPromocoderCategoriesVendors';
  public $primaryKeyField = 'code_id';

}

return 'msPromocoderCategoriesVendorsGetProcessor';