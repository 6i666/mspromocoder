<?php

/**
 * Seve categories/vendors attached to promocode.
 */
class msPromocoderCategoriesVendorsUpdateProcessor extends modObjectUpdateProcessor {
  public $objectType = 'msPromocoderCategoriesVendors';
  public $classKey = 'msPromocoderCategoriesVendors';
  public $primaryKeyField = 'code_id';

}

return 'msPromocoderCategoriesVendorsUpdateProcessor';